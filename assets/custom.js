$(function() { // DOM ready
    var openedSelect; // keep track of opened Select boxes

    $(".cselect").each(function() {
        var $input = $(this).find("input");
        var $dropDown = $(this).find("div.csdropdown");

        $(this).on("click", function() {
            $dropDown.slideToggle();
            $(this).children('.csdropdown').find("input#searchFilter").focus();
            // $("#searchFilter").focus();

        });

        $dropDown.on("click", "li", function() {
            $input.val($(this).text());
        });

    });
});